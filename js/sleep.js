var slide = 1;
showPic(slide);

function slideNo(n) {
  showPic(slide += n);
}

function showPic(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {
    slide = 1
  }
  if (n < 1) {
    slide = x.length
  };
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slide - 1].style.display = "block";
  var div = document.getElementById("div");
  var text = "";
  if (n == 0) text += "<br></br><b>Reason 8:</b> <p> Good Sleep also leads to clear thinking</p>"
  if (n == 1) text += "<br></br><b>Reason 1:</b> <p> Poor Sleep Can Make You Fat</p>"
  if (n == 2) text += "<br></br><b>Reason 2:</b> <p> Good Sleepers Tend to Eat Fewer Calories</p>"
  if (n == 3) text += "<br></br><b>Reason 3:</b> <p> Good Sleep Can Improve Concentration and Productivity</p>"
  if (n == 4) text += "<br></br><b>Reason 4:</b> <p> Good Sleep Can Maximize Athletic Performance</p>"
  if (n == 5) text += "<br></br><b>Reason 5:</b> <p> Sleep Improves Your Immune Function</p>"
  if (n == 6) text += "<br></br><b>Reason 6:</b> <p> Sleep Affects Emotions and Social Interactions</p>"
  if (n == 7) text += "<br></br><b>Reason 7:</b> <p> Good Sleep lead to a better mood</p>"
  if (n == 8) text += "<br></br><b>Reason 8:</b> <p> Good Sleep also leads to clear thinking</p>"
  if (n == 9) text += "<br></br><b>Reason 1:</b> <p> Poor Sleep Can Make You Fat</p>"
  div.innerHTML = text;
}
