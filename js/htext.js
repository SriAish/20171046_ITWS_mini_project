function changeText(value) {
  var div = document.getElementById("div");
  var text = "";
  if (value == 1) text = "\
        <h2>1. The Chilling Discovery</h2> \
\
        <p>There are multiple versions of this story, and most of them start with a young college girl who was studying late, and thus was spending a lot of time in the library instead of her dorm room. During a late night of studying, she realized that she had forgotten something in her dorm room, so she decided to make a trip back to go get it. When she opened the door she found the room dark, but figured that her roommate was either asleep or out studying like her. Not wanting to disturb her in case she was asleep, she left the light off, grabbed what she needed, and went back to the library.</p>\
\
<p>Upon returning to her room, she found her roommate lying on the floor with a slit throat. But the worst part was the message written in lipstick on the bathroom mirror that read simply, <q>Aren't you glad you didn't turn on the light?</q></p>\
\
<p>In another version of the tale, there is a woman who lives alone in an apartment with her puppy. She was fast asleep one night when she was awoken by a strange noise. To reassure herself, she reached down to where the pup slept beside the bed and felt it lick her hand. Satisfied, she drifted peacefully back to sleep. The next morning she discovered that the dog had been hanged in the shower. On the floor beside her bed was a note that said, <q>Humans can lick too.</q></p>  ";
  if (value == 2) text = "<h2>2. The Scream Nobody Heard</h2>\
\
<p>At some college campuses, it's apparently a tradition for students who live in dorms to all let loose with a scream at a designated time.</p> \
\
<p>According to the stories, it helps the students release stress, especially during finals week. On finals week at UCLA, the tradition was for everybody to scream at midnight to let out all that pent-up frustration. So, just as expected, everyone did their screaming ritual and the campus rang with the caffeine-fueled howls of a horde of exhausted youths.</p>\
\
<p>The ritual complete, the campus quieted down and everyone eventually went to bed, only to discover the next morning that one of the screams had been real. A young woman had been raped at precisely midnight, her assault timed to coincide with the noisiest moment of the year.</p><p>Nobody heard it, because what's one scream among hundreds? Legend has it that since then, the screaming tradition has been banned from the UCLA campus, and anyone who breaks the rule is punished with expulsion.<p> ";
  if (value == 3) text = "<h2>3. I hate it when my brother Charlie has to go away</h2>\
<p>I hate it when my brother Charlie has to go away.</p>\
\
<p>My parents constantly try to explain to me how sick he is. That I am lucky for having a brain where all the chemicals flow properly to their destinations like undammed rivers. When I complain about how bored I am without a little brother to play with, they try to make me feel bad by pointing out that his boredom likely far surpasses mine, considering his confine to a dark room in an institution.</p>\
\
<p>I always beg for them to give him one last chance. Of course, they did at first. Charlie has been back home several times, each shorter in duration than the last. Every time without fail, it all starts again. The neighbourhood cats with gouged out eyes showing up in his toy chest, my dad's razors found dropped on the baby slide in the park across the street, mom's vitamins replaced by bits of dishwasher tablets. My parents are hesitant now, using <q>last chances</q> sparingly. They say his disorder makes him charming, makes it easy for him to fake normalcy, and to trick the doctors who care for him into thinking he is ready for rehabilitation. That I will just have to put up with my boredom if it means staying safe from him.</p>\
\
<p>I hate it when Charlie has to go away. It makes me have to pretend to be good until he is back.</p>";
  if (value == 4) text = "<h2>4. Seeing Red</h2>\
<p>Everyone loves the first day of school, right? New year, new classes, new friends. It's a day full of potential and hope, before all the dreary depressions of reality show up to ruin all the fun.</p>\
\
<p>I like the first day of school for a different reason, though. You see, I have a sort of power. When I look at people, I can...sense a sort of aura around them. A colored outline based on how long that person has to live. Most everyone I meet around my age is surrounded by a solid green hue, which means they have plenty of time left.</p>\
\
<p>A fair amount of them have a yellow-orangish tinge to their auras, which tends to mean a car crash or some other tragedy. Anything that takes people <q>before their time</q> as they say.</p>\
\
<p>The real fun is when the auras venture into the red end of the spectrum, though. Every now and again I'll see someone who's basically a walking stoplight. Those are the ones who get murdered or kill themselves. It's such a rush to see them and know their time is numbered.</p>\
\
<p>With that in mind, I always get to class very early so I can scout out my classmates' fates. The first kid who walked in was basically radiating red. I chuckled to myself. Too damn bad, bro. But as people kept walking in, they all had the same intense glow. I finally caught a glimpse of my rose-tinted reflection in the window, but I was too stunned to move. Our professor stepped in and locked the door, his aura a sickening shade of green.</p>";
  div.innerHTML = text;
}
